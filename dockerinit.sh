docker-compose build
docker-compose up -d
docker-compose run php php bin/console -n doctrine:migrations:migrate
docker-compose run php php bin/console -n doctrine:fixtures:load
docker-compose run php app/console doctrine:schema:update --force
