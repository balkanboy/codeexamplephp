<?php

namespace Tests\TsfBundle\Domain\Service;

use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Tests\TsfBundle\TsfTestCase;
use TsfBundle\Domain\Service\CRUDService;
use TsfBundle\Domain\Service\PointService;
use TsfBundle\Entity\Category;
use TsfBundle\Entity\Point;
use TsfBundle\Entity\Tag;
use TsfBundle\Entity\User;
use TsfBundle\Repository\BaseRepository;

class PointServiceTest extends TsfTestCase
{
    /**
     * @var PointService
     */
    protected $service;
    
    /**
     * @var MockInterface | BaseRepository
     */
    protected $mockRepository;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockCrudService;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockCategoryCrudService;
       
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockTagCrudService;
   
    /**
     * @var MockInterface | TokenStorage
     */
    protected $mockTokenStorage;
    
    protected function setUp()
    {
        $this->mockRepository = \Mockery::mock(BaseRepository::class);
        $this->mockCrudService = \Mockery::mock(CRUDService::class);
        $this->mockCategoryCrudService  = \Mockery::mock(CRUDService::class);
        $this->mockTagCrudService  = \Mockery::mock(CRUDService::class);
        $this->mockTokenStorage  = \Mockery::mock(TokenStorage::class);
        
        $this->service = new PointService(...$this->getConstructorInjection());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getConstructorInjection()
    {
        return [
            $this->mockCrudService,
            $this->mockRepository,
            $this->mockCategoryCrudService,
            $this->mockTagCrudService,
            $this->mockTokenStorage
        ];
    }
    
    public function testPreCreate()
    {
        $created = new \DateTime;
        
        $token = new PreAuthenticatedToken('anon.', 'apiKey', 'providerKey');
        $token->setUser((new User())->setId(456));
        
        $point = (new Point())->setComment('point1');
        $category = (new Category())->setName('category1')->setId('123');
        $reloadedCategory = (new Category())->setName('category1')->setId('123')->setCreated($created);
       
        $tag = (new Tag())->setName('tag1')->setId(456);
        $reloadedTag = (new Tag())->setName('tag1')->setId(456)->setCreated($created);
        
        $point
            ->addTag($tag)
            ->setCategory($category);
        
        $this->mockTokenStorage
            ->shouldReceive('getToken')
            ->once()
            ->with()
            ->andReturn($token);
        
        $this->mockCategoryCrudService
            ->shouldReceive('retrieve')
            ->once()
            ->with(123)
            ->andReturn($reloadedCategory);
        
        $this->mockTagCrudService
            ->shouldReceive('retrieve')
            ->once()
            ->with(456)
            ->andReturn($reloadedTag);
        
        $refreshedEntity = $this->service->preCreate($point);
        
        $this->assertEquals($reloadedTag, $refreshedEntity->getTags()->first());
        $this->assertEquals($reloadedCategory, $refreshedEntity->getCategory());        
    }
    
    public function testPreUpdate()
    {
        $created = new \DateTime;
        $category = (new Category())->setName('category1')->setId(123)->setCreated($created);       
       
        $tag1 = (new Tag())->setName('tag3')->setId(123);
        $tag2 = (new Tag())->setName('tag4')->setId(456);
        $requestTag = (new Tag())->setName('tag2')->setId(123);
        
        // point from request
        $point = (new Point())
            ->setCategory((new Category())->setId(123))
            ->setComment('point1')
            ->setId(456)
            ->addTag($requestTag);
        
        // point from db
        $reloadedPoint = (new Point())
            ->setCategory($category)
            ->setComment('point1')
            ->setId(456)
            ->setCreated($created)
            ->addTag($tag1)
            ->addTag($tag2);
        
        $this->mockCategoryCrudService
            ->shouldReceive('retrieve')
            ->once()
            ->with('123')
            ->andReturn($category);
     
        $this->mockCrudService
            ->shouldReceive('update')
            ->once()
            ->with($point)
            ->andReturn($reloadedPoint);
        
        $this->mockTagCrudService
            ->shouldReceive('upsert')
            ->once()
            ->with($requestTag)
            ->andReturn($requestTag);
        
        $this->mockTagCrudService
            ->shouldReceive('delete')
            ->once()
            ->with($tag2)
            ->andReturn($tag2);
        
        $refreshedPoint = $this->service->preUpdate($point);
        
        $this->assertCount(1, $refreshedPoint->getTags());
        $this->assertEquals('tag2', $refreshedPoint->getTags()->first()->getName());
        $this->assertEquals($created, $refreshedPoint->getCategory()->getCreated());
    }
}