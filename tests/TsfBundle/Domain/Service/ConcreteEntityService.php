<?php

namespace Tests\TsfBundle\Domain\Service;

use TsfBundle\Domain\Service\AbstractEntityService;
use TsfBundle\Entity\AbstractEntity;

/**
 * Concrete service used to test AbstractEntityService
 */
class ConcreteEntityService extends AbstractEntityService
{
    public function preCreate(AbstractEntity $entity)
    {
        return $entity;
    }

    public function preUpdate(AbstractEntity $entity)
    {
        return $entity;
    }
}
