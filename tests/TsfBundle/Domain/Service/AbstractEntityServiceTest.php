<?php

namespace Tests\TsfBundle\Domain\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Tests\TsfBundle\TsfTestCase;
use TsfBundle\Domain\Service\AbstractEntityService;
use TsfBundle\Domain\Service\CRUDService;
use TsfBundle\Entity\Category;
use TsfBundle\Entity\Tag;
use TsfBundle\Repository\BaseRepository;

class AbstractEntityServiceTest extends TsfTestCase
{
    /**
     * @var AbstractEntityService
     */
    protected $service;
    
    /**
     * @var MockInterface | BaseRepository
     */
    protected $mockRepository;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockCrudService;
    
    protected function setUp()
    {
        $this->mockRepository = \Mockery::mock(BaseRepository::class);
        $this->mockCrudService = \Mockery::mock(CRUDService::class);
        
        $this->service = new ConcreteEntityService(...$this->getConstructorInjection());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getConstructorInjection()
    {
        return [
            $this->mockCrudService,
            $this->mockRepository
        ];
    }
    
    /**
     * Make sure pre-create hook is called.
     */
    public function testCreate()
    {
        $service = $this->getPartialMock(ConcreteEntityService::class, ['preCreate']);
        $tag = new Tag();
        
        $service->shouldReceive('preCreate')
            ->once()
            ->with($tag)
            ->andReturn($tag);
        
        $this->mockCrudService
            ->shouldReceive('save')
            ->once()
            ->with(\Mockery::mustBe($tag))
            ->andReturn($tag);
        
        $this->service->create($tag);
        
        // No assertions, we check only expectations.
        $this->assertTrue(true);
    }
    
    /**
     * Make sure pre-update hook is called.
     */
    public function testUpdate()
    {
        $service = $this->getPartialMock(ConcreteEntityService::class, ['preUpdate']);
        $tag = new Tag();
        
        $service->shouldReceive('preUpdate')
            ->once()
            ->with($tag)
            ->andReturn($tag);
        
        $this->mockCrudService
            ->shouldReceive('save')
            ->once()
            ->with(\Mockery::mustBe($tag))
            ->andReturn($tag);
        
        $this->service->create($tag);
        
        // No assertions, we check only expectations.
        $this->assertTrue(true);
    }
    
    /**
     * Test delete entity, if expectations are fulfilled.
     */
    public function testDelete()
    {
        $service = $this->getPartialMock(AbstractEntityService::class, ['getUuid']);
        $uuidString = '7d584aa713b941d99abf6f30c44d03ae';
        $tag = (new Tag())->setId($uuidString);
        
        $service->shouldReceive('getUuid')
            ->once()
            ->with($tag)
            ->andReturn($uuidString);
        
        $this->mockRepository
            ->shouldReceive('find')
            ->once()
            ->with($uuidString)
            ->andReturn($tag);
        
        $this->mockCrudService
            ->shouldReceive('delete')
            ->once()
            ->with($tag)
            ->andReturn($tag);
        
        $this->mockCrudService
            ->shouldReceive('save')
            ->once()
            ->with($tag, false)
            ->andReturn($tag);
        
        $service->delete($tag);
        
        // No assertions, we check only expectations.
        $this->assertTrue(true);
    }
    
    /**
     * Test sub-entities data synchronization. After method execution, sub-entities
     * related to main entity should be equal to request entities.
     */
    public function testSyncSubEntities()
    {
        $method = $this->getPublicMethod($this->service, 'syncSubEntities');
        $subEntityCrudService = \Mockery::mock(CRUDService::class); 
        $entity = new Category(); 
        
        $tag1 = (new Tag())->setId(1)->setName('tag1');
        $tag2 = (new Tag())->setId(1)->setName('tag2');
        $tag3 = (new Tag())->setId(2)->setName('tag3');
        
        $requestEntities = new ArrayCollection();
        $requestEntities->add($tag1);
        
        $currentEntities = new ArrayCollection();
        $currentEntities->add($tag2);
        $currentEntities->add($tag3);
        
        $subEntityCrudService
            ->shouldReceive('upsert')
            ->once()
            ->with(\Mockery::mustBe($tag1))
            ->andReturn((clone $tag2)->setName($tag1->getName()));
        
        $subEntityCrudService
            ->shouldReceive('delete')
            ->once()
            ->with(\Mockery::mustBe($tag3))
            ->andReturn($tag3);

        $resultSubEntities = $method->invoke(
            $this->service,
            $subEntityCrudService,
            $entity,
            $requestEntities,
            $currentEntities
        );
        
        $this->assertCount(1, $resultSubEntities);
        $this->assertEquals($tag1->getName(), reset($resultSubEntities)->getName());
    }
}