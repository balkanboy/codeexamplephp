<?php

namespace Tests\TsfBundle\Domain\Service;

use Tests\TsfBundle\TsfTestCase;
use TsfBundle\Domain\Service\AbstractEntityService;
use TsfBundle\Domain\Service\CategoryService;
use TsfBundle\Domain\Service\CRUDService;
use TsfBundle\Entity\Board;
use TsfBundle\Entity\Category;
use TsfBundle\Entity\Point;
use TsfBundle\Entity\Tag;
use TsfBundle\Repository\BaseRepository;

class CategoryServiceTest extends TsfTestCase
{
    /**
     * @var CategoryService
     */
    protected $service;
    
    /**
     * @var MockInterface | BaseRepository
     */
    protected $mockRepository;
       
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockTagCrudService;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockPointCrudService;
    
    /**
     * @var MockInterface | AbstractEntityService
     */
    protected $mockPointService;

    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockBoardCrudService;
    
    protected function setUp()
    {
        $this->mockRepository = \Mockery::mock(BaseRepository::class);
        $this->mockCrudService = \Mockery::mock(CRUDService::class);
        $this->mockTagCrudService  = \Mockery::mock(CRUDService::class);
        $this->mockPointCrudService  = \Mockery::mock(CRUDService::class);
        $this->mockPointService  = \Mockery::mock(AbstractEntityService::class);
        $this->mockBoardCrudService  = \Mockery::mock(CRUDService::class);
        
        $this->service = new CategoryService(...$this->getConstructorInjection());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getConstructorInjection()
    {
        return [
            $this->mockCrudService,
            $this->mockRepository,
            $this->mockTagCrudService,
            $this->mockPointCrudService,
            $this->mockPointService,
            $this->mockBoardCrudService
        ];
    }
    
    public function testPreCreate()
    {
        $created = new \DateTime;
        
        $board = (new Board())->setName('board1')->setId(123);
        $reloadedBoard = (new Board())->setName('board1')->setId(123)->setCreated($created);
        $category = ((new Category())->setName('category1'));
        $tag = (new Tag())->setName('tag1');
        $point = (new Point())->setComment('point1');
        
        $category->setBoard($board)
            ->addTag($tag)
            ->addPoint($point);
        
        $this->mockBoardCrudService
            ->shouldReceive('retrieve')
            ->once()
            ->with(123)
            ->andReturn($reloadedBoard);
        
        $refreshedEntity = $this->service->preCreate($category);
        
        $this->assertEquals($tag, $refreshedEntity->getTags()->first());
        $this->assertEquals($point, $refreshedEntity->getPoints()->first());
        $this->assertEquals($category, $refreshedEntity->getTags()->first()->getCategory());
        $this->assertEquals($category, $refreshedEntity->getPoints()->first()->getCategory());
        $this->assertEquals($reloadedBoard, $refreshedEntity->getPoints()->first()->getCategory()->getBoard());
        
    }
    
    public function testPreUpdate()
    {
        $created = new \DateTime;
        
        $board = (new Board())->setName('board1')->setId(123);
        $reloadedBoard = (new Board())->setName('board1')->setId(123)->setCreated($created);
        $category = ((new Category())->setName('category1'));
        
        // category from db
        $reloadedCategory = (new Category())
            ->setBoard($reloadedBoard)
            ->setId(123)
            ->setName('category1')
            ->setCreated($created)
            ->addTag((new Tag())->setName('tag2'))
            ->addTag((new Tag())->setName('tag3'))
            ->addPoint((new Point())->setComment('point2'));
        
        // category from request
        $category->setId('123')
            ->setBoard($board)
            ->addTag((new Tag())->setName('tag1'))
            ->addPoint((new Point())->setComment('point1'));
        
        $this->mockBoardCrudService
            ->shouldReceive('retrieve')
            ->once()
            ->with('123')
            ->andReturn($reloadedBoard);
     
        $this->mockCrudService
            ->shouldReceive('update')
            ->once()
            ->with($category)
            ->andReturn($reloadedCategory);
        
        $refreshedCategory = $this->service->preUpdate($category);
        
        $this->assertCount(1, $refreshedCategory->getTags());
        $this->assertEquals('tag1', $refreshedCategory->getTags()->first()->getName());
        $this->assertCount(1, $refreshedCategory->getPoints());
        $this->assertEquals('point1', $refreshedCategory->getPoints()->first()->getComment());
        $this->assertEquals($created, $refreshedCategory->getBoard()->getCreated());
    }
    
    /**
     * Somehow it is extremely hard to mock parent method call in Mockery.
     * Therefore, parent method call ->delete() is not mocked,
     * but it's expectations are also specified here and it makes
     * this test not an unit test. Next time try Prophecy, it might be
     * possible to mock parent class calls.
     */
    public function testDelete()
    {
        /** If I use FQCN it messes the php imports, this way should work since class is auto-loaded **/
        $mockParent = \Mockery::mock('overload:AbstractEntityService');
        
        $mockService = $this->getPartialMock(CategoryService::class, ['getUuid']);
        $created = new \DateTime;
        $uuidString = '7d584aa713b941d99abf6f30c44d03ae';
        $tag = (new Tag())->setName('tag1');
        $reloadedTag = (new Tag())->setName('tag1')->setCreated($created);
        $point = (new Point())->setComment('point1')->setId(456);
        $reloadedPoint = (new Point())->setComment('point1')->setCreated($created);
        
        $reloadedCategory = (new Category())
             ->setId($uuidString)
             ->setName('category1')
             ->addTag($tag)
             ->addPoint($point)
             ->setCreated($created);
        
        $this->mockCrudService
                ->shouldReceive('retrieve')
                ->once()
                ->with($uuidString)
                ->andReturn($reloadedCategory);
        
        // Expectations for point deletion
        $this->mockPointCrudService
                ->shouldReceive('retrieve')
                ->once()
                ->with($point->getId())
                ->andReturn($reloadedPoint);
        
        $this->mockPointCrudService
                ->shouldReceive('delete')
                ->once()
                ->with($reloadedPoint)
                ->andReturn($reloadedPoint);
        
        $this->mockPointCrudService
                ->shouldReceive('save')
                ->once()
                ->with($reloadedPoint, false)
                ->andReturn($reloadedPoint);
        
        // Expectations for tag deletion
        $this->mockTagCrudService
                ->shouldReceive('retrieve')
                ->once()
                ->with($tag->getId())
                ->andReturn($reloadedTag);
        
        $this->mockTagCrudService
                ->shouldReceive('delete')
                ->once()
                ->with($reloadedTag)
                ->andReturn($reloadedTag);
        
        $this->mockTagCrudService
                ->shouldReceive('save')
                ->once()
                ->with($reloadedTag, false)
                ->andReturn($reloadedTag);
        
        // Somehow this is not working in Mockery. Eventhough the parent method call is 
        // mocked, it is still executed. 
        $mockParent->shouldReceive('delete')
            ->once()
            ->with(\Mockery::mustBe($reloadedCategory))
            ->andReturn(\Mockery::mustBe($reloadedCategory));
        
        // So we specify expectations within the parent delete method 8-o
        $mockService->shouldReceive('getUuid')
            ->once()
            ->with($reloadedCategory)
            ->andReturn($uuidString);
        
        $this->mockRepository
            ->shouldReceive('find')
            ->once()
            ->with($uuidString)
            ->andReturn($reloadedCategory);
        
        $this->mockCrudService 
            ->shouldReceive('delete')
            ->once()
            ->with($reloadedCategory)
            ->andReturn($reloadedCategory);
        
        $this->mockCrudService 
            ->shouldReceive('save')
            ->once()
            ->with($reloadedCategory, false)
            ->andReturn($reloadedCategory);
        
        $refreshedCategory = $mockService->delete($reloadedCategory);
        $this->assertEquals('tag1', $refreshedCategory->getTags()->first()->getName());
        $this->assertEquals('point1', $refreshedCategory->getPoints()->first()->getComment());
    }
}