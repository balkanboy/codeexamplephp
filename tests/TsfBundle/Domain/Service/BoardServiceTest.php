<?php

namespace Tests\TsfBundle\Domain\Service;

use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Tests\TsfBundle\TsfTestCase;
use TsfBundle\Domain\Service\AbstractEntityService;
use TsfBundle\Domain\Service\BoardService;
use TsfBundle\Domain\Service\CRUDService;
use TsfBundle\Entity\Board;
use TsfBundle\Entity\Category;
use TsfBundle\Entity\User;
use TsfBundle\Repository\BaseRepository;

class BoardServiceTest extends TsfTestCase
{
    /**
     * @var BoardService
     */
    protected $service;
    
    /**
     * @var MockInterface | BaseRepository
     */
    protected $mockRepository;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockCrudService;
    
    /**
     * @var MockInterface | AbstractEntityService
     */
    protected $mockCategoryService;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockCategoryCrudService;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockUserCrudService;
    
    /**
     * @var MockInterface | CRUDServiceInterface
     */
    protected $mockTokenStorage;
    
    protected function setUp()
    {
        $this->mockRepository = \Mockery::mock(BaseRepository::class);
        $this->mockCrudService = \Mockery::mock(CRUDService::class);
        $this->mockCategoryService  = \Mockery::mock(AbstractEntityService::class);
        $this->mockCategoryCrudService  = \Mockery::mock(CRUDService::class);
        $this->mockUserCrudService  = \Mockery::mock(CRUDService::class);
        $this->mockTokenStorage  = \Mockery::mock(TokenStorage::class);
        
        $this->service = new BoardService(...$this->getConstructorInjection());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getConstructorInjection()
    {
        return [
            $this->mockCrudService,
            $this->mockRepository,
            $this->mockCategoryService,
            $this->mockCategoryCrudService,
            $this->mockUserCrudService,
            $this->mockTokenStorage
        ];
    }
    
    public function testPreCreate()
    {
        $category1 = ((new Category())->setName('category1'));
        
        $board = (new Board())
            ->setName('board1')
            ->addCategory($category1);
        
        $reloadedCategory = (new Category())
            ->setName('category1')
            ->setBoard($board);
        
        $token = new PreAuthenticatedToken('anon.', 'apiKey', 'providerKey');
        $token->setUser((new User())->setId(456));
        
        $this->mockTokenStorage
            ->shouldReceive('getToken')
            ->once()
            ->with()
            ->andReturn($token);
        
        $this->mockCategoryService
            ->shouldReceive('preCreate')
            ->once()
            ->with($category1)
            ->andReturn($reloadedCategory);
        
        $refreshedBoard = $this->service->preCreate($board);
        $this->assertEquals('board1', $refreshedBoard->getCategories()->first()->getBoard()->getName());
        $this->assertEmpty($refreshedBoard->getCollaborators());
    }
 
    public function testPreCreate_withCollaborators()
    {
        $created = new \DateTime;
        $user = (new User())->setName('user1')->setId(111)->setCreated(new \DateTime('yesterday'));
        $reloadedUser = (new User())->setName('user1')->setId(111)->setCreated($created);
        
        $board = (new Board())
            ->setName('board1')
            ->addCollaborator($user);
        
        $token = new PreAuthenticatedToken('anon.', 'apiKey', 'providerKey');
        $token->setUser((new User())->setId(456));
        
        $this->mockTokenStorage
            ->shouldReceive('getToken')
            ->once()
            ->with()
            ->andReturn($token);
        
        $this->mockUserCrudService
            ->shouldReceive('retrieve')
            ->once()
            ->with(111)
            ->andReturn($reloadedUser);
        
        $refreshedBoard = $this->service->preCreate($board);
        $this->assertEquals($created, $refreshedBoard->getCollaborators()->first()->getCreated());
        
        $this->assertTrue(true);
    }
    
    public function testPreUpdate()
    {
        $created = new \DateTime;
        $board = (new Board())->setName('board1')->setId(123);
        $category = ((new Category())->setName('category1'));
        
        $reloadedBoard = (new Board())
            ->setName('board1')
            ->setId(123)
            ->setCreated($created)
            ->addCategory($category);
        
        $reloadedCategory = (new Category())
            ->setName('category1')
            ->setBoard($reloadedBoard);
                
        $this->mockCrudService 
            ->shouldReceive('update')
            ->once()
            ->with($board)
            ->andReturn($reloadedBoard);
        
                
        $this->mockCategoryCrudService 
            ->shouldReceive('preUpdate')
            ->once()
            ->with($category)
            ->andReturn($reloadedCategory);
        
        $refreshedBoard = $this->service->preUpdate($board);
        
        $this->assertEquals('board1', $refreshedBoard->getName());
        $this->assertEquals('category1', $refreshedBoard->getCategories()->first()->getName());
        $this->assertEquals($created, $refreshedBoard->getCreated());
    }
    
    /**
     * Somehow it is extremely hard to mock parent method call in Mockery.
     * Therefore, parent method call ->delete() is not mocked,
     * but it's expectations are also specified here and it makes
     * this test not an unit test. Next time try Prophecy, it might be
     * possible to mock parent class calls.
     * 
     * See CategoryServiceTest->testDelete() 
     */
    public function testDelete()
    {
        $service = $this->getPartialMock(BoardService::class, ['getUuid']);
        
        $board = (new Board())->setName('board1')->setId(123);
        $category = ((new Category())->setName('category1'));
        
        $reloadedBoard = (new Board())
            ->setName('board1')
            ->setId(123)
            ->addCategory($category);
        
        $reloadedCategory = (new Category())
            ->setName('category1')
            ->setBoard($reloadedBoard);

        $service->shouldReceive('getUuid')
            ->once()
            ->with($reloadedBoard)
            ->andReturn('123');
        
        $this->mockCrudService 
            ->shouldReceive('retrieve')
            ->once()
            ->with(123)
            ->andReturn($reloadedBoard);

        $this->mockRepository
            ->shouldReceive('find')
            ->once()
            ->with(123)
            ->andReturn($reloadedBoard);
        
        $this->mockCrudService 
            ->shouldReceive('delete')
            ->once()
            ->with($reloadedBoard)
            ->andReturn($reloadedBoard);
        
        $this->mockCrudService 
            ->shouldReceive('save')
            ->once()
            ->with($reloadedBoard, false)
            ->andReturn($reloadedBoard);
    
        $this->mockCategoryCrudService 
            ->shouldReceive('delete')
            ->once()
            ->with($category)
            ->andReturn($reloadedCategory);
        
        $refreshedBoard = $service->delete($board);
        $this->assertEquals('board1', $refreshedBoard->getName());
        $this->assertEquals('category1', $refreshedBoard->getCategories()->first()->getName());
    }
}