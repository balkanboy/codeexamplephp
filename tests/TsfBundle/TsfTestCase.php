<?php

namespace Tests\TsfBundle;

use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Helper methods for all test cases.
 */
class TsfTestCase extends TestCase
{
    /**
     * Allow invocation of protected methods in tests.
     * 
     * @param mixed $instance
     * @param string $name
     * @return mixed
     */
    public function getPublicMethod($instance, $name)
    {
        $class = new ReflectionClass($instance);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        
        return $method;
    }
    
    /**
     * Return constructor arguments. Test classes that use partial service
     * mock should override this and return corresponding constructor args.
     * 
     * @return array
     */
    public function getConstructorInjection()
    {
        return [];
    }
    
    /**
     * Get partial mock of service to be tested.
     * 
     * @param string $className
     * @param array $methodsToMock
     * @return MockInterface
     */
    public function getPartialMock($className, array $methodsToMock)
    {
        return 
            \Mockery::mock(
                $className . '[' . implode(',', $methodsToMock) . ']',
                $this->getConstructorInjection()
            )
            ->shouldAllowMockingProtectedMethods();
    }
}
