<?php

namespace TsfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Constraint;
use TsfBundle\Entity\Category;

/**
 * @ORM\Table(name="board")
 * @ORM\Entity(repositoryClass="TsfBundle\Repository\BoardRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Board extends AbstractEntity 
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     * 
     * @Constraint\NotBlank(groups={"post"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("description")
     * 
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     * 
     * @JMS\Type("boolean")
     * @JMS\SerializedName("active")
     * 
     */
    protected $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="public", type="boolean",nullable=true)
     * 
     * @JMS\Type("boolean")
     * @JMS\SerializedName("public")
     * 
     */
    protected $public = true;

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("imageUrl")
     * 
     */
    protected $imageUrl;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Category",mappedBy="board",cascade={"persist"})
     * 
     * @JMS\Type("array<TsfBundle\Entity\Category>")
     * @JMS\SerializedName("categories")
     * 
     */
    protected $categories;

    /**
     * @var TsfBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User",inversedBy="boards")
     */
    protected $owner;
    
    /**
     * @var TsfBundle\Entity\User
     * 
     * @ORM\ManyToMany(targetEntity="User",mappedBy="boardsCollaborated")
     */
    protected $collaborators;
    
    /**
     * @var TsfBundle\Entity\User
     * 
     * @ORM\ManyToMany(targetEntity="User",mappedBy="boardsAdministered")
     */
    protected $admins;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->collaborators = new ArrayCollection();
        $this->admins = new ArrayCollection();
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * 
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * 
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * 
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }
    
    /**
     * 
     * @param string $imageUrl
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @param Category $category
     * @return User
     */
    public function addCategory(Category $category)
    {
        $category->setBoard($this);
        $this->categories[] = $category;

        return $this;
    }

    /**
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * @return Collection
     */
    public function getCategories()
    {
        return !empty($this->categories) ? $this->categories : new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function clearCategories()
    {
        $this->categories = new ArrayCollection();
    }    

    /**
     * 
     * @return bool
     */
    public function getPublic()
    {
        return $this->public;
    }
    
    /**
     * 
     * @param bool $public
     * @return $this
     */
    public function setPublic($public)
    {
        $this->public = $public;
        return $this;
    }
    
    /**
     * 
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * 
     * @param User $owner
     * @return $this
     */
    public function setOwner(User $owner) 
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @param User $collaborator
     * @return $this
     */
    public function addCollaborator(User $collaborator)
    {
        $collaborator->addBoardCollaborated($this);
        $this->collaborators[] = $collaborator;

        return $this;
    }

    /**
     * @param User $collaborator
     * @return $this
     */
    public function removeCollaborator(User $collaborator)
    {
        $this->collaborators->removeElement($collaborator);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCollaborators()
    {
        return !empty($this->collaborators) ? $this->collaborators : new ArrayCollection;
    }
    
    /**
     * 
     * @return $this
     */
    public function clearCollaborators()
    {
        $this->collaborators = new ArrayCollection();
        return $this;
    }
    
    /**
     * @param \TsfBundle\Entity\User $admin
     *
     * @return User
     */
    public function addAdmin(User $admin)
    {
        $admin->addBoardAdministered($this);
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * 
     * @param User $admin
     * @return $this
     */
    public function removeAdmin(User $admin)
    {
        $this->admins->removeElement($admin);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getAdmins()
    {
        return !empty($this->admins) ? $this->admins : new ArrayCollection();
    }
    
    /**
     * @return $this
     */
    public function clearAdmins()
    {
        $this->admins = new ArrayCollection();
        return $this;
    }
}
