<?php

namespace TsfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Constraint;

/**
 * @ORM\Table(name="point")
 * @ORM\Entity(repositoryClass="TsfBundle\Repository\PointRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Point extends AbstractEntity 
{
    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=1024, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("comment")
     * 
     */
    protected $comment;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Tag",cascade={"persist", "remove"})
     * 
     * @JMS\Type("array<TsfBundle\Entity\Tag>")
     * @JMS\SerializedName("tags")
     */
    protected $tags;

    /**
     * @var TsfBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User",inversedBy="points",cascade={"persist"})
     * 
     */
    protected $user;

    /**
     * @var TsfBundle\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="Category",inversedBy="points")
     * 
     * @JMS\Type("TsfBundle\Entity\Category")
     * @JMS\SerializedName("category")
     * 
     * @Constraint\NotBlank(groups={"post"})
     */
    protected $category;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }
    
    /**
     * 
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * 
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * 
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @param User $user
     * @return $this
     */
    public function setUser(User $user) 
    {
        $this->user = $user;
        return $this;
    }

    /**
     * 
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * 
     * @param Category $category
     * @return $this
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * 
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }
    
    /**
     * 
     * @param Tag $tag
     * @return $this
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTags()
    {
        return !empty($this->tags) ? $this->tags : new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function setTags($tags)
    {
        foreach ($tags as $tag) {
            $this->addTag($tag);
        }
        return $this;
    }
    
    /**
     * 
     * @return $this
     */
    public function clearTags()
    {
        $this->tags = new ArrayCollection();
        return $this;
    }
}
