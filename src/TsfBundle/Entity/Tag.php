<?php

namespace TsfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="TsfBundle\Repository\TagRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Tag extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     * 
     * @Assert\NotBlank(groups={"post"})
     */
    protected $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="defaultTag", type="boolean")
     * 
     * @JMS\Type("boolean")
     * @JMS\SerializedName("default")
     */
    protected $default = false;

    /**
     * @var TsfBundle\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="Category",inversedBy="tags",cascade={"persist"})
     * 
     * * @Assert\NotBlank(groups={"post"})
     */
    protected $category;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Point",cascade={"persist"})
     */
    protected $points;

    public function __construct()
    {
        $this->points = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Identity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set default
     *
     * @param string $default
     *
     * @return Identity
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set default
     *
     * @param integer $default
     *
     * @return Identity
     */
    public function setActive($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return int
     */
    public function getActive()
    {
        return $this->default;
    }

    /**
     * @param Point $point
     *
     * @return User
     */
    public function addPoint(Point $point)
    {
        $this->points[] = $point;

        return $this;
    }

    /**
     * @param Point $point
     * @return $this
     */
    public function removePoint(Point $point)
    {
        $this->points->removeElement($point);
        return $this;
    }

    /**
     * @return $this
     */
    public function setPoints($points)
    {
        foreach ($points as $point) {
            $this->addPoint($point);
        }
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * 
     * @return Category
     */
    public function getCategory()
    {
        return !empty($this->category) ? $this->category : new Category();
    }   
    
    /**
     * 
     * @param Category $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
}
