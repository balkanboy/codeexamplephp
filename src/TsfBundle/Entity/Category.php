<?php

namespace TsfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Constraint;

/**
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="TsfBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Category extends AbstractEntity 
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     * 
     * @Constraint\NotBlank(groups={"post"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("description")
     * 
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     * 
     * @JMS\Type("boolean")
     * @JMS\SerializedName("active")
     * 
     */
    protected $active = true;

    /**
     * @var int
     *
     * @ORM\Column(name="orderPosition", type="boolean", nullable=true)
     * 
     * @JMS\Type("int")
     * @JMS\SerializedName("orderPosition")
     * 
     */
    protected $orderPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("imageUrl")
     * 
     */
    protected $imageUrl;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Tag",mappedBy="category",cascade={"persist"})
     * 
     * @JMS\Type("ArrayCollection<TsfBundle\Entity\Tag>")
     * @JMS\SerializedName("tags")
     */
    protected $tags;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Point",mappedBy="category",cascade={"persist"})
     * 
     * @JMS\Type("array<TsfBundle\Entity\Point>")
     * @JMS\SerializedName("points")
     */
    protected $points;

    /**
     * @var Board
     *
     * @ORM\ManyToOne(targetEntity="Board",inversedBy="categories",cascade={"persist"})
     * 
     * @JMS\Type("TsfBundle\Entity\Board")
     * @JMS\SerializedName("board")
     * 
     * @Constraint\NotBlank(groups={"post"})
     */
    protected $board;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->points = new ArrayCollection();
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * 
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * 
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * 
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getOrderPosition()
    {
        return $this->orderPosition;
    }
    
    /**
     * 
     * @param int $orderPosition
     * @return $this
     */
    public function setOrderPosition($orderPosition)
    {
        $this->orderPosition = $orderPosition;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }
    
    /**
     * 
     * @param string $imageUrl
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @param Tag $tag
     *
     * @return $this
     */
    public function addTag(Tag $tag)
    {
        $tag->setCategory($this);
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * 
     * @return $this
     */
    public function clearTags()
    {
        $this->tags = new ArrayCollection();
        return $this;
    }

    /**
     *
     * @return Collection
     */
    public function getTags()
    {
        return !empty($this->tags) ? $this->tags : new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function setTags($tags)
    {
        foreach ($tags as $tag) {
            $this->addTag($tag);
        }
        return $this;
    }

    /**
     * @param Point $point
     *
     * @return $this
     */
    public function addPoint(Point $point)
    {
        $point->setCategory($this);
        $this->points[] = $point;

        return $this;
    }

    /**
     * 
     * @return $this
     */
    public function clearPoints()
    {
        $this->points = new ArrayCollection();
        return $this;
    }
    
    /**
     * @return $this
     */
    public function setPoints($points)
    {
        foreach ($points as $point) {
            $this->addPoint($point);
        }
        return $this;
    }

    /**
     * @param Point $point
     */
    public function removePoint(Point $point)
    {
        $this->points->removeElement($point);
    }

    /**
     * @return Collection
     */
    public function getPoints()
    {
        return !empty($this->points) ? $this->points : new ArrayCollection();
    }

    /**
     * 
     * @return Board
     */
    public function getBoard()
    {
        return $this->board;
    }
    
    /**
     * 
     * @param Board $board
     * @return $this
     */
    public function setBoard($board)
    {
        $this->board = $board;
        return $this;
    }
}
