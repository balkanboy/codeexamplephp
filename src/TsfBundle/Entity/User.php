<?php

namespace TsfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Constraint;
use JMS\Serializer\Annotation as JMS;
use TsfBundle\Service\Validator\Constraint as TSFAssert;

/**
 * @ORM\Table(name="usero")
 * @ORM\Entity(repositoryClass="TsfBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends AbstractEntity implements UserInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("email")
     * 
     * @Constraint\NotBlank(groups={"post"})
     * @Constraint\Email(groups={"post"})
     * @TSFAssert\EmailUniqueConstraint(groups={"post"})
     */
    protected $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     * 
     * @Constraint\NotBlank(groups={"post"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("password")
     * 
     * @Constraint\NotBlank(groups={"post"})
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("firstName")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("lastName")
     */
    protected $lastName;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active = 1;

    /**
    * @ORM\ManyToMany(targetEntity="UserRole",inversedBy="users")
    */
    protected $userRoles;

    /**
    * @ORM\ManyToOne(targetEntity="Team",inversedBy="users")
    * @Constraint\NotBlank()
    */
    protected $team;
    
    /**
     * @var TsfBundle\Entity\Category
     *
     * @ORM\OneToMany(targetEntity="Point",mappedBy="user",cascade={"persist"})
     * 
     */
    protected $points;
    
    /**
     * @var TsfBundle\Entity\Board[]
     *
     * @ORM\OneToMany(targetEntity="Board",mappedBy="owner",cascade={"persist"})
     * 
     */
    protected $boardsOwned;
    
    /**
     * @var TsfBundle\Entity\User
     * 
     * @ORM\JoinTable(name="user_collaborator_board")
     * @ORM\ManyToMany(targetEntity="Board",inversedBy="collaborators")
     */
    protected $boardsCollaborated;
    
    /**
     * @var TsfBundle\Entity\User
     * 
     * @ORM\JoinTable(name="user_admin_board")
     * @ORM\ManyToMany(targetEntity="Board",inversedBy="admins")
     */
    protected $boardsAdministered;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
        $this->boardsOwned = new ArrayCollection();
        $this->boardsCollaborated = new ArrayCollection();
        $this->boardsAdministered = new ArrayCollection();
    }

    /**
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * 
     * @return string
     */
    public function getName() 
    {
        return $this->name;
    }

    /**
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name) 
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     *
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     *
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     *
     * @param boolean $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * 
     * @return array
     */
    public function getRoles()
    {
        return $this->getRolesArray();
    }
    
    /**
     * 
     * @param string $roleName
     * @return bool
     */
    public function hasRole($roleName)
    {
        return in_array($roleName, $this->getRolesArray());
    }
    
    /**
     * 
     * @return array
     */
    protected function getRolesArray()
    {
        $roles = [];
        
        foreach ($this->userRoles as $userRole) {
            $roles[] = $userRole->getName();
        }
        
        return $roles;
    }

    /**
     * 
     * @return string
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * 
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     *
     * @param \TsfBundle\Entity\UserRole $userRole
     * @return $this
     */
    public function addUserRole(UserRole $userRole)
    {
        $this->userRoles[] = $userRole;
        return $this;
    }

    /**
     *
     * @param UserRole $userRole
     */
    public function removeUserRole(UserRole $userRole)
    {
        $this->userRoles->removeElement($userRole);
    }
    
    /**
     * @param Board $board
     *
     * @return $this
     */
    public function addBoardOwned(Board $board)
    {
        $this->boardsOwned[] = $board;
        return $this;
    }

    /**
     * @param Board $board
     * @return $this
     */
    public function removeBoardOwned(User $board)
    {
        $this->boardsOwned->removeElement($board);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoardsOwned()
    {
        return !empty($this->boardsOwned) ? $this->boardsOwned : new ArrayCollection();
    }
    
    /**
     * @param Board $board
     *
     * @return User
     */
    public function addBoardCollaborated(Board $board)
    {
        $this->boardsCollaborated[] = $board;
        return $this;
    }

    /**
     * @param Board $board
     * @return $this
     */
    public function removeBoardCollaborated(User $board)
    {
        $this->boardsCollaborated->removeElement($board);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoardsCollaborated()
    {
        return !empty($this->boardsCollaborated) ? $this->boardsCollaborated : new ArrayCollection();
    }
    
    /**
     * 
     * @param Board $board
     * @return $this
     */
    public function addBoardAdministered(Board $board)
    {
        $this->boardsAdministered[] = $board;
        return $this;
    }

    /**
     * @param Board $board
     * @return $this;
     */
    public function removeBoardAdministered(User $board)
    {
        $this->boardsAdministered->removeElement($board);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoardsAdministered()
    {
        return !empty($this->boardsAdministered) ? $this->boardsAdministered : new ArrayCollection();
    }
    
    /**
     * Checks if the user has roles that make him regular user.
     * 
     * @return bool
     */
    public function isRegularUser()
    {
        return $this->hasRole(UserRole::ROLE_USER)
            && !$this->hasRole(UserRole::ROLE_MANAGER)
            && !$this->hasRole(UserRole::ROLE_ADMIN);
    }
    
    /**
     * Checks if user has admin privileges.
     * 
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole(UserRole::ROLE_ADMIN);
    }
    
    /**
     * Checks if user has manager privileges.
     * 
     * @return bool
     */
    public function isManager()
    {
        return $this->hasRole(UserRole::ROLE_MANAGER);
    }
}
