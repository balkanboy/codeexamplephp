<?php

namespace TsfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Constraint;

/*
 * Abstract business entity. Provides common functionalities like automatic
 * creation date, sync of related entities, etc.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 * 
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
class AbstractEntity
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     *
     * @Constraint\NotBlank(groups={"put,delete"})
     * @Constraint\Uuid
     */
    protected $id;
    
    /**
    * @ORM\Column(name="created", type="datetime")
    */
    protected $created;

    /**
     * Triggered on insert
     * 
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        // $this->created = new \DateTime("now");

        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new \DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        $this->created = $d;
    }
    
    /**
     * 
     * @return string
     */
    public function getId()
    {
        return str_replace('-', '', (string) $this->id);
    }
    
    /**
     * 
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * 
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Synchronize state of related entities. Here it's a little bit of magic
     * right way would be to have this method abstract and implement 
     * entity copy in child entity classes.
     * 
     * @param \TsfBundle\Entity\AbstractEntity $entity
     * @return $this
     */
    public function syncValues(AbstractEntity $entity)
    {               
        $properties = get_object_vars($this);

        foreach ($properties as $property => $value) {
            if ($property == 'id') {
                continue;
            }

            $getterMethod = 'get' . ucfirst($property);
            $setterMethod = 'set' . ucfirst($property);
            
            if (!method_exists($this, $setterMethod)) {
                continue;
            }
            
            if ($entity->{$getterMethod}() !== null
                && $entity->{$getterMethod}() != $this->{$getterMethod}()
            ) {
                $this->{$setterMethod}($entity->{$getterMethod}());
            }
        }

        return $this;
    }

    /**
     * Synchronize state of related entities from array.
     * 
     * @param array $data
     * @return $this
     */
    public function syncValuesFromArray(array $data)
    {
        $properties = get_object_vars($this);
        
        foreach ($properties as $property => $value) {
            if ($property == 'id') {
                continue;
            }

            $getterMethod = 'get' . ucfirst($property);
            $setterMethod = 'set' . ucfirst($property);
            
            if (isset($data[$property])
                && $data[$property] != $this->{$getterMethod}()
            ) {
                $this->{$setterMethod}($data[$property]);
            }
        }

        return $this;
    }
    
    /**
     * Static helper to check if id is in valid UUID format.
     * @param type $id
     * @return boolean
     */
    public static function isValidUuid($id)
    {
      if (preg_match('/^(\{)?[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}(?(1)\})$/i', $id)) {
        return true;
      }

      if (preg_match('/^[a-f\d]{32}$/i', $id)) {
        return true;
      }

      return false;
    }

}
