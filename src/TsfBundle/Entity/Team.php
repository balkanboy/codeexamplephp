<?php

namespace TsfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Constraint;

/**
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="TsfBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Team extends AbstractEntity 
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     * 
     * @Constraint\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * 
     * @JMS\Type("string")
     * @JMS\SerializedName("description")
     * 
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     * 
     * @JMS\Type("boolean")
     * @JMS\SerializedName("active")
     * 
     */
    protected $active;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="User",mappedBy="team")
     * 
     * @JMS\Type("array<TsfBundle\Entity\User>")
     * @JMS\SerializedName("users")
     */
    protected $users;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Board")
     * 
     * @JMS\Type("array<TsfBundle\Entity\Board>")
     * @JMS\SerializedName("boards")
     */
    protected $boards;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->boards = new ArrayCollection();
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * 
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * 
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * 
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(\TsfBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param Board $board
     *
     * @return $this
     */
    public function addBoard(\TsfBundle\Entity\Board $board)
    {
        $this->boards[] = $board;
        return $this;
    }

    /**
     * @param Board $board
     * @return $this
     */
    public function removeBoard(\TsfBundle\Entity\Board $board)
    {
        $this->boards->removeElement($board);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoints()
    {
        return $this->points;
    }
}
