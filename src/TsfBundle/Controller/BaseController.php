<?php

namespace TsfBundle\Controller;

use TsfBundle\Entity\AbstractEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TsfBundle\Service\Exception\InputValidationException;

/**
 * Base controller provides convenience methods that can be used by all controllers.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class BaseController extends Controller
{
    CONST FORMAT_JSON = 'json';
    CONST FORMAT_XML = 'xml';

    /**
     * 
     * @return APIResponseRenderer
     */
    protected function getResponseRenderer()
    {
        return $this->get('app.api_response');
    }

    /**
     * 
     * @param Request $request
     * @param string $entityClass
     * @return AbstractEntity
     */
    protected function deserializeRequest(Request $request, $entityClass)
    {
        $serializer = $this->get('jms_serializer');

        return $serializer->deserialize(
            $request->getContent(), 
            $entityClass, 
            self::FORMAT_JSON
        );
    }

    /**
     * 
     * @param AbstractEntity $entity
     * @param array $groups
     * @throws InputValidationException
     */
    protected function validate(AbstractEntity $entity, array $groups)
    {
        $validator = $this->get('validator');
        $errors = $validator->validate($entity, null, $groups);

        if (count($errors) > 0) {
            throw new InputValidationException((string) $errors);
        }
    }
}
