<?php

namespace TsfBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use TsfBundle\Domain\Service\CRUDServiceInterface;
use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Service\Exception\IdInvalidException;
use TsfBundle\Service\Response\CollectionResponse;
use TsfBundle\Service\Transformer\AbstractTransformer;

/**
 * Entity controller basic CRUD operations with CRUD service interface.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class EntityController extends BaseController
{
    const VALIDATION_GROUP_POST = 'post';
    const VALIDATION_GROUP_PUT = 'put';
    const VALIDATION_GROUP_DELETE = 'delete';

    /**
     * @var CRUDServiceInterface
     */
    protected $crudService;
    
    /**
     * @var type AbstractTransformer
     */
    protected $transformer;
    
    public function __construct(
            CRUDServiceInterface $crudService, 
            AbstractTransformer $transformer
    ) {
        $this->crudService = $crudService;
        $this->transformer = $transformer;
    }
    
    public function create(AbstractEntity $entity)
    {
        $this->validate($entity, [self::VALIDATION_GROUP_POST]);
        return $this->renderItem($this->crudService->create($entity));
    }

    public function update(AbstractEntity $entity)
    {
        $this->validate($entity, [self::VALIDATION_GROUP_PUT]);
        $entity = $this->crudService->update($entity);

        return $this->renderItem($this->crudService->save($entity));
    }

    public function patchAction(Request $request, $id)
    {
        $fields = json_decode($request->getContent(), true);
        $entity = $this->crudService->patch($id, $fields);
        $entity = $this->crudService->save($entity);

        return $this->renderItem($entity);
    }

    public function retrieveAction($id)
    {
        if (!AbstractEntity::isValidUuid($id)) {
            throw IdInvalidException::forId($id);
        }
        
        $entity = $this->crudService->retrieve($id);

        return $this->renderItem($entity);
    }

    public function searchAction(Request $request)
    {
        return $this->renderCollection(
            $this->crudService->search(
                $request->get('query', null),
                $request->get('page', 1)
            )
        );
    }

    public function delete(AbstractEntity $entity)
    {
        $this->validate($entity, [self::VALIDATION_GROUP_DELETE]);
        $this->crudService->delete($entity);
        $this->crudService->save($entity, false);
        
        return $this->renderItem($entity);
    }
    
    protected function renderItem(AbstractEntity $entity)
    {
        return $this->getResponseRenderer()->renderItem(
            $entity,
            $this->transformer
        );
    }
    
    protected function renderCollection(CollectionResponse $collection)
    {
        return $this->getResponseRenderer()->renderCollection(
            $collection, 
            $this->transformer
        );
    }
}
