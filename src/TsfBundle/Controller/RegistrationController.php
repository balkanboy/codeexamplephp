<?php

namespace TsfBundle\Controller;

use TsfBundle\Entity\User;
use TsfBundle\Entity\UserRole;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends EntityController
{
    /**
     * @Route("/signup")
     * @Method({"POST","OPTIONS"})
     */
    public function signupAction(Request $request)
    {
        $user = $this->deserializeRequest($request, User::class);
        $this->validate($user, [self::VALIDATION_GROUP_POST]);
        
        $user->setPassword(password_hash($user->getPassword(), PASSWORD_BCRYPT, ['cost' => 13]));
        
        // Add default role
        $roleRepository = $this->get('TsfBundle\Repository\UserRoleRepository');
        $user->addUserRole($roleRepository->findOneByName(UserRole::ROLE_ADMIN));

        return $this->create($user);
    }
}
