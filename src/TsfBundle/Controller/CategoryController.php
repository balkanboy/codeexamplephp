<?php

namespace TsfBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TsfBundle\Entity\Board;
use TsfBundle\Entity\Point;

class CategoryController extends EntityController
{
    /**
     * @Route("/api/categories")
     * @Method({"POST","OPTIONS"})
     */
    public function createAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->create($entity);
    }

    /**
     * @Route("/api/categories")
     * @Method({"PUT","OPTIONS"})
     */
    public function updateAction(Request $request)
    { 
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->update($entity);
    }

    /**
     * @Route("/api/categories")
     * @Method({"DELETE","OPTIONS"})
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->delete($entity);
    }
    
    /**
     * @Route("/api/categories/{id}/tags")
     * @Method({"GET","OPTIONS"})
     */
    public function getTagsAction(Request $request, $id)
    {
        return $this->search(
            $request->get('page', 1),
            $request->get('query', null)
        );
    }
    /**
     * @Route("/api/categories/{id}/points")
     * @Method({"GET","OPTIONS"})
     */
    public function getPointsAction(Request $request, $id)
    {
        return $this->search(
            $request->get('page', 1),
            Point::class,
            ['category' => $id],
            false
        );
    }
    

}
