<?php

namespace TsfBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TsfBundle\Entity\Board;
use TsfBundle\Entity\Point;

class PointController extends EntityController
{
    /**
     * @Route("/api/points")
     * @Method({"POST","OPTIONS"})
     */
    public function createAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Point::class);
        return $this->create($entity);
    }

    /**
     * @Route("/api/points")
     * @Method({"PUT","OPTIONS"})
     */
    public function updateAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->update($entity);
    }

    /**
     * @Route("/api/points")
     * @Method({"DELETE","OPTIONS"})
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->delete($entity);
    }

}
