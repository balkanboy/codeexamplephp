<?php

namespace TsfBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TsfBundle\Entity\Tag;

class TagController extends EntityController
{
    /**
     * @Route("/api/tags")
     * @Method({"POST","OPTIONS"})
     */
    public function createAction(Request $request)
    {
        $categoryCrudService = $this->get('tsfive.domain.service.categoryCrudService');
        $tag = $this->deserializeRequest($request, Tag::class);
        $this->validate($tag, [self::VALIDATION_GROUP_POST]);
        $tag->setCategory($categoryCrudService->getEntity($tag->getCategory()->getId()));

        return $this->create($tag);
    }

    /**
     * @Route("/api/tags")
     * @Method({"PUT","OPTIONS"})
     */
    public function updateAction(Request $request)
    {
        return $this->update(
            $this->deserializeRequest($request, Tag::class)
        );
    }
    
    /**
     * @Route("/api/tags")
     * @Method({"DELETE","OPTIONS"})
     */
    public function deleteAction(Request $request)
    {
        $tag = $this->deserializeRequest($request, Tag::class);
        $this->validate($tag, [self::VALIDATION_GROUP_DELETE]);
        $tagCrudService = $this->get('tsfive.domain.service.tagCrudService');
        
        return $this->delete(
            $tagCrudService->getEntity($tag->getId())
        );
    }

}
