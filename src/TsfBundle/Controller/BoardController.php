<?php

namespace TsfBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Entity\Board;
use TsfBundle\Service\Exception\IdInvalidException;
use TsfBundle\Service\Response\CollectionResponse;
use TsfBundle\Service\Transformer\CategoryTransformer;

class BoardController extends EntityController
{
    /**
     * @Route("/api/boards")
     * @Method({"POST","OPTIONS"})
     */
    public function createAction(Request $request)
    {              
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->create($entity);
    }

    /**
     * @Route("/api/boards")
     * @Method({"PUT","OPTIONS"})
     */
    public function updateAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->update($entity);
    }

    /**
     * @Route("/api/boards")
     * @Method({"DELETE","OPTIONS"})
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->deserializeRequest($request, Board::class);
        return $this->delete($entity);
    }
    
    /**
     * @Route("/api/boards/{id}/categories")
     * @Method({"GET","OPTIONS"})
     */
    public function getCategoriesAction(Request $request, $id)
    {     
        if (!AbstractEntity::isValidUuid($id)) {
            throw IdInvalidException::forId($id);
        }
        
        $crudService = $this->get('tsfive.domain.service.boardCrudService');
        $categoryTransformer = $this->get(CategoryTransformer::class);
        $board = $crudService->getEntity($id);
        
        $categories = (new CollectionResponse())
                ->setCollection($board->getCategories())
                ->setPage($request->get('page', 1))
                ->setTotal(count($board->getCategories()));
        
        return $this->getResponseRenderer()->renderCollection($categories, $categoryTransformer);
    }

}
