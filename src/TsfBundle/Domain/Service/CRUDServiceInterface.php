<?php

namespace TsfBundle\Domain\Service;

use TsfBundle\Entity\AbstractEntity;

/**
 * Interface for CRUD operations on AbstractEntity. 
 * We want to be able to have simple CRUD service which can work
 * with entities without relations. Also, we want to be able
 * to implement some more complex logic in entity services,
 * but keep the same interface, so we can inject both
 * into controllers and make controllers aware only
 * of CRUDInterface and not the logic behind.
 * 
 * @author   Miroslav Jovanovic <mj7455@gmail.com>
 */
interface CRUDServiceInterface
{
    public function create(AbstractEntity $entity);
    
    public function retrieve($id);
    
    public function update(AbstractEntity $entity);
    
    public function delete(AbstractEntity $entity);
    
    public function upsert(AbstractEntity $entity);
    
    public function patch($id, array $fields);
    
    public function search($queryString, $pageNumber);
}
