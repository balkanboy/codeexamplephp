<?php

namespace TsfBundle\Domain\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Repository\BaseRepository;

/**
 * Domain service that provides CRUD functions for Board entity.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class BoardService extends AbstractEntityService
{
    /**
     * @var CRUDServiceInterface
     */
    protected $categoryCrudService;

    /**
     * @var CRUDServiceInterface
     */
    protected $categoryService;
    
    /**
     * @var CRUDServiceInterface
     */
    protected $userCrudService;
    
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    public function __construct(
        CRUDService $crudService,
        BaseRepository $entityRepository,
        AbstractEntityService $categoryService,
        CRUDService $categoryCrudService,
        CRUDService $userCrudService,
        TokenStorage $tokenStorage
    ) {
        parent::__construct(
            $crudService,
            $entityRepository
        );
        $this->categoryService = $categoryService;
        $this->categoryCrudService = $categoryCrudService;
        $this->userCrudService = $userCrudService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $board
     * @return AbstractEntity
     */
    public function preCreate(AbstractEntity $board)
    {
        $board->setOwner($this->tokenStorage->getToken()->getUser());
        
        foreach ($board->getCategories() as $category) {
            $this->categoryService->preCreate($category);
            $category->setBoard($board);
        }
        
        if (!empty($board->getCollaborators())) {
            $editors = $board->getCollaborators();
            $board->clearCollaborators();

            foreach ($editors as $editor) {
                $board->addCollaborator($this->userCrudService->retrieve($editor->getId()));
            }
        } else {
            $board->clearCollaborators();
        }

        return $board;
    }

    /**
     * {@inheritdoc}
     * 
     * We allow updating related categories passed in board update request.
     * However, categories that exist on the board and are not passed with request
     * will not be deleted.Deleting category only allowed through its 
     * dedicated endpoint in categories API.
     * 
     * @param AbstractEntity $board
     * @return type
     */
    public function preUpdate(AbstractEntity $board)
    {     
        $categories = $board->getCategories();
        $board = $this->crudService->update($board);

        foreach ($categories as $category) {
            $category = $this->categoryService->preUpdate($category);
        }

        return $board;
    }
    
    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $entity
     * @return type
     */
    public function delete(AbstractEntity $entity) 
    {
        $entity = $this->crudService->retrieve($entity->getId());
        
        foreach ($entity->getCategories() as $category) {
            $this->categoryCrudService->delete($category);
        }
        
        return parent::delete($entity);
    }
}