<?php

namespace TsfBundle\Domain\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TsfBundle\Entity\AbstractEntity;

/**
 * Simple CRUD service. Aimed to be used for simple entities that have no relations.
 * Could be also wrapped by entity services that require more complex logic,
 * in which case this service provides basic CRUD functionality.
 * 
 * @author   Miroslav Jovanovic <mj7455@gmail.com>
 */
class CRUDService implements CRUDServiceInterface
{
    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var EntityManager
     */
    protected $entityManager;
    
    /**
     *
     * @var TokenStorage
     */
    protected $tokenStorage;

    public function __construct(
        EntityRepository $repository,
        EntityManager $entityManager,
        TokenStorage $tokenStorage
    ) {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    public function create(AbstractEntity $entity)
    {
        //var_dump($this->tokenStorage->getToken()->getUser()->getId());
        return $this->save($entity);
    }
    
    public function retrieve($id)
    {
        $uuid = Uuid::fromString($id);
        return $this->repository->find((string) $uuid);
    }

    public function update(AbstractEntity $entity)
    {
        $uuid = Uuid::fromString($entity->getId());
        $oldEntity = $this->repository->find((string) $uuid);
        return $oldEntity->syncValues($entity);
    }
    
    public function delete(AbstractEntity $entity)
    {
        $this->entityManager->remove($entity);
        return $entity;
    }

    public function upsert(AbstractEntity $entity)
    {
        if (!AbstractEntity::isValidUuid($entity->getId())) {
            return $entity;
        }
        
        $uuid = Uuid::fromString($entity->getId());
        $getEntity = $this->repository->find((string) $uuid);
        
        if (empty($getEntity)) {
            return $entity;
        }
        
        return $getEntity->syncValues($entity);
    }

    public function patch($id, array $fields)
    {
        $uuid = Uuid::fromString($id);
        $entity = $this->repository->find((string) $uuid);
        
        $entity->syncValuesFromArray($fields);
        
        return $entity;
    }
    
    public function search($queryString, $pageNumber)
    {        
        return $this->repository->searchByUser(
            $this->tokenStorage->getToken()->getUser(),
            $queryString, 
            $pageNumber
        );
    }

//    public function syncEntities(array $entityIds)
//    {
//        $existingEntities = $this->repository->findById($enityIds);
//    }

    public function save(AbstractEntity $entity, $withPersist = true)
    {
        if ($withPersist) {
            $this->entityManager->persist($entity);
        }
        //\Doctrine\Common\Util\Debug::dump($entity);
        $this->entityManager->flush();

        return $entity;
    }
}