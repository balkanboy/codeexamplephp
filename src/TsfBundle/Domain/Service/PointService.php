<?php

namespace TsfBundle\Domain\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Repository\BaseRepository;

/**
 * Domain service that provides CRUD functions for Point entity.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class PointService extends AbstractEntityService
{
    /**
     * @var CRUDInterface
     */
    protected $crudCategoryService;

    /**
     * @var CRUDInterface
     */
    protected $crudTagService;
    
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    public function __construct(
        CRUDService $crudService,
        BaseRepository $entityRepository,
        CRUDService $crudCategoryService,
        CRUDService $crudTagService,
        TokenStorage $tokenStorage
    ) {
        parent::__construct(
            $crudService,
            $entityRepository
        );
        $this->crudCategoryService = $crudCategoryService;
        $this->crudTagService = $crudTagService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $point
     * @return AbstractEntity
     */
    public function preCreate(AbstractEntity $point)
    {
        if (!empty($point->getCategory())) {
            $point->setCategory($this->crudCategoryService->retrieve($point->getCategory()->getId()));
        }
        
        $point->setUser($this->tokenStorage->getToken()->getUser());
        
        if (!empty($point->getTags())) {
            $tags = $point->getTags();
            $point->clearTags();

            foreach ($tags as $tag) {
                $point->addTag($this->crudTagService->retrieve($tag->getId()));
            }
        } else {
            $point->clearTags();
        }

        return $point;
    }

    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $point
     * @return AbstractEntity
     */
    public function preUpdate(AbstractEntity $point)
    {
        if ($point->getCategory() !== null) {
            $point->setCategory($this->crudCategoryService->retrieve($point->getCategory()->getId()));
        }
        
        $requestTags = $point->getTags();
        $point = $this->crudService->update($point);
        $currentTags = $point->getTags();
        
        $point->setTags(
            $this->syncSubEntities(
                $this->crudTagService, 
                $point->clearTags(), 
                $requestTags, 
                $currentTags
            )    
        );
        
        return $point;
    }
}