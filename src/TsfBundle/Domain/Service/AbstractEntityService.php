<?php

namespace TsfBundle\Domain\Service;

use Doctrine\Common\Collections\Collection;
use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Repository\BaseRepository;
use Ramsey\Uuid\Uuid;

/**
 * Provides hooks needed before standard CRUD operations and proxies calls to underlying CRUDService.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
abstract class AbstractEntityService implements CRUDServiceInterface
{
    /**
     * @var CRUDServiceInterface
     */
    protected $crudService;

    /**
     * @var BaseRepository
     */
    protected $entityRepository;

    public function __construct(
        CRUDService $crudService,
        BaseRepository $entityRepository
    ) {
        $this->crudService = $crudService;
        $this->entityRepository = $entityRepository;
    }
    
    /**
     * Pre-create hook. Refreshes relation graph.
     * This connects IDs form request to database entities and
     * Doctrine becomes aware of the relations in entity that is
     * being created.
     */
    public abstract function preCreate(AbstractEntity $entity);

    /**
     * Pre-update hook. Synchronizes data from entity request to database entity
     * and prepares update / insert / delete of related entities.
     */
    public abstract function preUpdate(AbstractEntity $entity);
    
    /**
     * 
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    public function create(AbstractEntity $entity)
    {
        $this->preCreate($entity);
        return $this->crudService->save($entity);
    }
    
    /**
     * Proxy method.
     * 
     * @param int $id
     * @return AbstractEntity
     */
    public function retrieve($id)
    {
        return $this->crudService->retrieve($id);
    }

    /**
     * 
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    public function update(AbstractEntity $entity)
    {
        $entity = $this->preUpdate($entity);
        return $this->crudService->save($entity);
    }

    /**
     * Delete entity from database. Specific services should override
     * this method to cleanup related entities before deletion.
     * 
     * @param AbstractEntity $entity
     * @return type
     */
    public function delete(AbstractEntity $entity)
    {
        $uuid = $this->getUuid($entity);
        $getEntity = $this->entityRepository->find((string) $uuid);
        
        $this->crudService->delete($getEntity);
        $this->crudService->save($entity, false);
        
        return $getEntity;
    }
    
    /**
     * Proxy method.
     * 
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    public function upsert(AbstractEntity $entity)
    {
        return $this->crudService->upsert($entity);
    }
    
    /**
     * Proxy method.
     * 
     * @param int $id
     * @param array $fields
     * @return AbstractEntity
     */
    public function patch($id, array $fields)
    {
        return $this->crudService->patch($id, $fields);
    }
    
    /**
     * Proxy method. 
     * 
     * @param string $queryString
     * @param int $pageNumber
     * @return AbstractEntity
     */
    public function search($queryString, $pageNumber)
    {
        return $this->crudService->search($queryString, $pageNumber);
    }
    
    /**
     * Proxy method.
     * 
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    public function save(AbstractEntity $entity)
    {
        return $this->crudService->save($entity);
    }
    
    /**
     * Synchronizes data of related entities. Sub-entities are reloaded,
     * the ones from request are updated and the one that are not in 
     * request are deleted. 
     * 
     * Implemented with context injection
     * to make it applicable to any entity - sub-entity relation. 
     */
    protected function syncSubEntities(
        CRUDService $subEntityCRUDService, 
        AbstractEntity $entity, 
        Collection $requestEntities,
        Collection $currentEntities
    )
    {       
        $resultEntities = [];
        $passedEntityIds = [];

        foreach ($requestEntities as $subEntity) {
            $passedEntityIds[] = (string) $subEntity->getId();
        }

        foreach ($currentEntities as $subEntity) {
            if (!empty($subEntity->getId()) && !in_array($subEntity->getId(), $passedEntityIds)) {  
                $subEntityCRUDService->delete($subEntity);
            }
        }

        foreach ($requestEntities as $subEntity) {
            if (!empty($subEntity->getId())) {
                $subEntity = $subEntityCRUDService->upsert($subEntity);
            }
            $resultEntities[] = $subEntity;
        }

        return $resultEntities;
    }
    
    /**
     * Static call 8-o - wrap to allow mocking :-) 
     * 
     * @param AbstractEntity $entity
     * @return Uuid
     */
    protected function getUuid($entity)
    {
        return Uuid::fromString($entity->getId());
    }
}
