<?php

namespace TsfBundle\Domain\Service;

use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Repository\BaseRepository;

/**
 * Domain service that provides CRUD functions for Category entity.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class CategoryService extends AbstractEntityService
{
    /**
     * @var CRUDInterface
     */
    protected $crudTagService;
    
    /**
     * @var CRUDInterface
     */
    protected $crudPointService;

    /**
     * @var AbstractEntityService
     */
    protected $pointService;
    
    /**
     * @var CRUDInterface
     */
    protected $crudBoardService;

    public function __construct(
        CRUDService $crudService,
        BaseRepository $entityRepository,
        CRUDService $crudTagService,
        CRUDService $crudPointService,
        AbstractEntityService $pointService,
        CRUDService $crudBoardService
    ) {
        parent::__construct(
            $crudService,
            $entityRepository
        );
        $this->crudTagService = $crudTagService;
        $this->crudPointService = $crudPointService;
        $this->pointService = $pointService;
        $this->crudBoardService = $crudBoardService;
    }

    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $category
     * @return AbstractEntity
     */
    public function preCreate(AbstractEntity $category)
    {       
        if ($category->getBoard() !== null) {
            $category->setBoard($this->crudBoardService->retrieve($category->getBoard()->getId()));
        }
        
        foreach ($category->getTags() as $tag) {
            $tag->setCategory($category);
        }

        foreach ($category->getPoints() as $point) {
            $point->setCategory($category);

            foreach ($point->getTags() as $pointTag) {
                $pointTag->addPoint($point);
                $pointTag->setCategory($category);
            }
        }

        return $category;
    }

    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $category
     * @return AbstractEntity
     */
    public function preUpdate(AbstractEntity $category)
    {
        if ($category->getBoard() !== null) {
            $category->setBoard($this->crudBoardService->retrieve($category->getBoard()->getId()));
        }
        
        $requestTags = $category->getTags();
        $requestPoints = $category->getPoints();
        $category = $this->crudService->update($category);
        $currentTags = $category->getTags();
        $currentPoints = $category->getPoints();
                
        $category->setTags(
            $this->syncSubEntities(
                $this->crudTagService, 
                $category->clearTags(), 
                $requestTags, 
                $currentTags
            )    
        );
        
        $category->setPoints(
            $this->syncSubEntities(
                $this->crudPointService, 
                $category->clearPoints(), 
                $requestPoints, 
                $currentPoints
            )    
        );
        
        return $category;       
    }
    
    /**
     * {@inheritdoc}
     * 
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    public function delete(AbstractEntity $entity) 
    {
        $entity = $this->crudService->retrieve($entity->getId());
        
        foreach ($entity->getPoints() as $point) {
            $point = $this->crudPointService->retrieve($point->getId());
            $this->crudPointService->delete($point);
            $this->crudPointService->save($point, false);
        }
        
        foreach ($entity->getTags() as $tag) {
            $tag = $this->crudTagService->retrieve($tag->getId());
            $this->crudTagService->delete($tag);
            $this->crudTagService->save($tag, false);
        }
        
        return parent::delete($entity);
    }
}