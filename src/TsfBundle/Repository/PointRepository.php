<?php

namespace TsfBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use TsfBundle\Entity\User;

class PointRepository extends BaseRepository
{
    /**
     * {@inheritdoc}
     */
    public function getQuery() 
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p, c, t FROM TsfBundle:Point p
                 LEFT JOIN p.category c 
                 LEFT JOIN p.tags t
                WHERE p.id = :id
                ORDER BY p.created DESC'
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSearchQuery()
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('e')
            ->from('TsfBundle:Point', 'e')
            ->leftJoin('e.category', 'c')
            ->leftJoin('c.board', 'b')
            ->leftJoin('b.collaborators', 'bc');

        return $qb;
    }
   
    /**
     * {@inheritdoc}
     */
    protected function applySearchCriteria(QueryBuilder $queryBuilder, $queryString)
    {
        $queryBuilder
            ->andWhere('(LOWER(e.comment) LIKE  LOWER(?1))')
            ->setParameter(1, '%' . $queryString . '%');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function applyUserFilter(User $user, QueryBuilder $queryBuilder)
    {
        if ($user->isRegularUser()) {
            $queryBuilder->andWhere('(b.owner = :id OR b.public = true AND bc.id = :id)');
            $queryBuilder->setParameter(':id', $user->getId());
        }
        
        return $queryBuilder;
    }
}
