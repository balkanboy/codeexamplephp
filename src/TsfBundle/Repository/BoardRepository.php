<?php

namespace TsfBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use TsfBundle\Entity\User;

class BoardRepository extends BaseRepository
{
    /**
     * {@inheritdoc}
     */
    public function getQuery() 
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT b, c, t, p FROM TsfBundle:Board b 
                 LEFT JOIN b.categories c
                 LEFT JOIN c.tags t
                 LEFT JOIN c.points p
                WHERE b.id = :id
                ORDER BY b.name ASC'
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSearchQuery()
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        
        $qb->select('e, bc')
            ->from('TsfBundle:Board', 'e')
            ->leftJoin('e.collaborators', 'bc');
        
        return $qb;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function applyUserFilter(User $user, QueryBuilder $queryBuilder)
    {
        if ($user->isRegularUser()) {
            $queryBuilder->andWhere('(e.owner = :id OR e.public = true AND bc.id = :id)');
            $queryBuilder->setParameter(':id', $user->getId());
        }
    }

}
