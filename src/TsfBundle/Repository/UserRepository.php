<?php

namespace TsfBundle\Repository;

use TsfBundle\Repository\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * {@inheritdoc}
     */
    public function getQuery() 
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT u, ur FROM TsfBundle:User u
                LEFT JOIN u.userRoles ur
                WHERE u.id = :id
                ORDER BY u.name ASC'
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSearchQuery() 
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('e')
            ->from('TsfBundle:User', 'e')
            ->leftJoin('e.userRole', 'ur');
        
        return $qb;
    }
}
