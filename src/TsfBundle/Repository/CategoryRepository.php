<?php

namespace TsfBundle\Repository;

use TsfBundle\Repository\BaseRepository;

class CategoryRepository extends BaseRepository
{
    /**
     * {@inheritdoc}
     */
    public function getQuery() 
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c, t, p FROM TsfBundle:Category c
                 LEFT JOIN c.tags t
                 LEFT JOIN c.points p
                WHERE c.id = :id
                ORDER BY c.name ASC'
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSearchQuery()
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('e')
            ->from('TsfBundle:Category', 'e')
            ->leftJoin('e.board', 'b')
            ->leftJoin('b.collaborators', 'bc');

        return $qb;
    }
}
