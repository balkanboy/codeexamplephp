<?php

namespace TsfBundle\Repository;

use Doctrine\ORM\QueryBuilder;

class TagRepository extends BaseRepository
{
    /**
     * {@inheritdoc}
     */
    public function getQuery() 
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t, c FROM TsfBundle:Tag t 
                 INNER JOIN t.category c
                WHERE t.id = :id
                ORDER BY t.name ASC'
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSearchQuery()
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('e')
            ->from('TsfBundle:Tag', 'e')
            ->leftJoin('e.category', 'c')
            ->leftJoin('c.board', 'b')
            ->leftJoin('b.collaborators', 'bc');

        return $qb;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function applySearchCriteria(QueryBuilder $queryBuilder, $queryString)
    {
        $queryBuilder
            ->andWhere('(LOWER(e.name) LIKE  LOWER(?1))')
            ->setParameter(1, '%' . $queryString . '%');
    }
}
