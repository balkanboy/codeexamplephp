<?php

namespace TsfBundle\Repository;

class UserRoleRepository extends BaseRepository
{
    /**
     * {@inheritdoc}
     */
    public function getQuery() 
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT ur FROM TsfBundle:UserRole ur
                WHERE ur.name = :name
                ORDER BY ur.name ASC'
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSearchQuery() 
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();

        $qb->select('e')
            ->from('TsfBundle:UserRole', 'e')
            ->leftJoin('e.user', 'u');
        
        return $qb;
    }
}
