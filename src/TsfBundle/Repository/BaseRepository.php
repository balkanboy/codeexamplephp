<?php

namespace TsfBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use TsfBundle\Entity\User;
use TsfBundle\Service\Exception\IdNotFoundException;
use TsfBundle\Service\Response\CollectionResponse;

/**
 * Provides methods common to all repositories and helper methods.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
abstract class BaseRepository extends \Doctrine\ORM\EntityRepository
{   
    /**
     * Implements query required to fetch specific entity.
     */
    public abstract function getQuery();
    
    /**
     * Implements search query for searhing entities.
     */
    public abstract function getSearchQuery();

    /**
     * 
     * @param string $id
     * @return AbstractEntity
     * @throws IdNotFoundException
     */
    public function find($id)
    {
        $entity = $this->getQuery()
            ->setParameter('id', $id)
            ->getOneOrNullResult();

        if (empty($entity)) {
            throw IdNotFoundException::forId($id);
        }

        return $entity;
    }
    
    /**
     * 
     * @param User $user
     * @param string $queryString
     * @param int $pageNo
     * @return CollectionResponse
     */
    public function searchByUser(User $user, $queryString, $pageNo = 1)
    {       
        if ($pageNo < 1) {
            $pageNo = 1;
        }
        
        $offset = ($pageNo - 1) * CollectionResponse::PAGE_SIZE;
        $qb = $this->getSearchQuery();
        
        $this->applySearchCriteria($qb, $queryString);   
        $this->applyUserFilter($user, $qb);
        
        //\Doctrine\Common\Util\Debug::dump($qb->getQuery()); die;
        $total = (clone $qb)->select('count(e.id)')->getQuery()->getSingleScalarResult(); 
        $qb->orderBy('e.created', 'DESC');
        
        $collection = new CollectionResponse();

        $collection->setCollection(
            $qb->getQuery()
                ->setFirstResult($offset)
                ->setMaxResults(CollectionResponse::PAGE_SIZE)
                ->getResult()
        );
        
        $collection->setTotal($total);
        $collection->setPage($pageNo); 
        
        return $collection;
    }
    
    /**
     * Adds where clause to query builder. This is common where clause, 
     * but some entities may want to override this and supply specific where clause.
     * 
     * @param QueryBuilder $queryBuilder
     * @param string $queryString
     */
    protected function applySearchCriteria(QueryBuilder $queryBuilder, $queryString)
    {
        $queryBuilder
            ->andWhere('(LOWER(e.name) LIKE  LOWER(?1) OR LOWER(e.description) LIKE LOWER(?1))')
            ->setParameter(1, '%' . $queryString . '%');
    }
    
    /**
     * Add where to limit results for specific user.
     * 
     * @param User $user
     * @param QueryBuilder $queryBuilder
     * @return QueryBuilder
     */
    protected function applyUserFilter(User $user, QueryBuilder $queryBuilder)
    {
        if ($user->isRegularUser()) {
            $queryBuilder->andWhere('(b.owner = :id OR b.public = true AND bc.id = :id)');
            $queryBuilder->setParameter(':id', $user->getId());
        }
        
        return $queryBuilder;
    }
}
