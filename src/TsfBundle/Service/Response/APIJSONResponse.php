<?php

namespace TsfBundle\Service\Response;

use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Service\Transformer\AbstractTransformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use TsfBundle\Service\Response\CollectionResponse;

/**
 * Format for JSON output response.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class APIJSONResponse implements APIResponse
{
    /**
     * {@inheritdoc}
     */
    public function renderItem(AbstractEntity $item, AbstractTransformer $transformer)
    {
        $data = $transformer->transform($item);
        $response = $this->getResponseTemplate(200, $data);

        return new JsonResponse($response);
    }

    /**
     * {@inheritdoc}
     */
    public function renderCollection(CollectionResponse $collection, AbstractTransformer $transformer)
    {
        $data = [];

        foreach ($collection->getCollection() as $item) {
            $data[] = $transformer->transformForCollection($item);
        }
        
        $metadata['total'] = $collection->getTotal();
        $metadata['page'] = $collection->getPage();
            
        $response = $this->getResponseTemplate(200, $data, $metadata);

        return new JsonResponse($response);
    }

    /**
     * {@inheritdoc}
     */
    public function renderError($errorCode, $errorMessage)
    {
        return new JsonResponse(
            self::getResponseTemplate(
                $errorCode,
                ['errors' => $errorMessage]
            )
        );
    }

    /**
     * Template for API response.
     * 
     * @param type $status
     * @param type $data
     * @param type $metadata
     * @return type
     */
    protected function getResponseTemplate($status, $data, $metadata = null)
    {
        $response = [
            'status' => $status,
            'data' => $data
        ];
        
        if ($metadata !== null) {
            $response['metadata'] = $metadata;
        }
        
        return $response;
    }
}