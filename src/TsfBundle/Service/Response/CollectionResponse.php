<?php

namespace TsfBundle\Service\Response;

use Doctrine\Common\Collections\Collection;

/**
 * Model for rendering collection to response.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class CollectionResponse
{
    const PAGE_SIZE = 50;
    
    /** 
     * @var Collection 
     */
    protected $collection;
    
    /**
     *
     * @var int
     */
    protected $page = 0;
    
    /**
     *
     * @var int
     */
    protected $total;
    
    /**
     * 
     * @return Collection
     */
    function getCollection()
    {
        return $this->collection;
    }

    /**
     * 
     * @return int
     */
    function getPage()
    {
        return $this->page;
    }

    /**
     * 
     * @return int
     */
    function getTotal() 
    {
        return $this->total;
    }

    /**
     * 
     * @param Collection $collection
     * @return $this
     */
    function setCollection($collection) 
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * 
     * @param int $page
     * @return $this
     */
    function setPage($page) 
    {
        $this->page = (int) $page;
        return $this;
    }

    /**
     * 
     * @param int $total
     * @return $this
     */
    function setTotal($total) 
    {
        $this->total = $total;
        return $this;
    } 
}
