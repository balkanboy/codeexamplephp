<?php

namespace TsfBundle\Service\Response;

use TsfBundle\Entity\AbstractEntity;
use TsfBundle\Service\Response\CollectionResponse;
use TsfBundle\Service\Transformer\AbstractTransformer;

/**
 * API output rendering classes must implement this interface.
 * 
 * @author   Miroslav Jovanovic <mj7455@gmail.com>
 */
interface APIResponse
{
    /**
     * Create response for single object.
     * 
     * @param AbstractEntity $item
     * @param AbstractTransformer $transformer
     */
    public function renderItem(AbstractEntity $item, AbstractTransformer $transformer);

    /**
     * Create response for collection of objects.
     * 
     * @param CollectionResponse $collection
     * @param AbstractTransformer $transformer
     */
    public function renderCollection(CollectionResponse $collection, AbstractTransformer $transformer);

    /**
     * Provide HTTP error response.
     * 
     * @param type $errorCode
     * @param type $errorMessage
     */
    public function renderError($errorCode, $errorMessage);
}