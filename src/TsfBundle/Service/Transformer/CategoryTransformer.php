<?php

namespace TsfBundle\Service\Transformer;

use TsfBundle\Entity\AbstractEntity;

class CategoryTransformer extends AbstractTransformer
{
    /**
     * @var AbstractTransformer
     */
    protected $tagTransformer;

    protected $pointTransformer;

    public function __construct(
        AbstractTransformer $tagTransformer,
        AbstractTransformer $pointTransformer
    )
    {
        $this->tagTransformer = $tagTransformer;
        $this->pointTransformer = $pointTransformer;
        $this->pointTransformer->setEmbedParent(false);
    }

    public function transform(AbstractEntity $category)
    {
        $result = [];

        $result['id'] = $this->formatUuid($category->getId());
        $result['name'] = $category->getName();
        $result['description'] = $category->getDescription();
        $result['active'] = $category->getActive();
        $result['orderPosition'] = $category->getOrderPosition();
        $result['imageUrl'] = $category->getImageUrl();
        $result['tags'] = [];
        $result['points'] = [];

        foreach ($category->getTags() as $tag) {
            $result['tags'][] = $this->tagTransformer->transform($tag);
        }

        foreach ($category->getPoints() as $point) {
            $result['points'][] = $this->pointTransformer->transform($point);
        }
        
        $result['created'] = $category->getCreated()->format(\DateTime::ATOM);

        return $result;
    }
    
    public function transformForCollection(AbstractEntity $category)
    {
        $result = [];

        $result['id'] = $this->formatUuid($category->getId());
        $result['name'] = $category->getName();
        $result['description'] = $category->getDescription();
        $result['active'] = $category->getActive();
        $result['orderPosition'] = $category->getOrderPosition();
        $result['imageUrl'] = $category->getImageUrl();
        $result['created'] = $category->getCreated()->format(\DateTime::ATOM);

        return $result;
    }
}
