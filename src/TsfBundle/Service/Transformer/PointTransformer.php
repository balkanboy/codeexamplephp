<?php

namespace TsfBundle\Service\Transformer;

use TsfBundle\Entity\AbstractEntity;

class PointTransformer extends AbstractTransformer
{
    /**
     * @var AbstractTransformer
     */
    protected $tagTransformer;
    
    /**
     *
     * @var UserTransformer
     */
    protected $userTransformer;

    public function __construct(AbstractTransformer $tagTransformer, AbstractTransformer $userTransformer)
    {
        $this->tagTransformer = $tagTransformer;
        $this->tagTransformer->setEmbedParent(false);
        
        $this->userTransformer = $userTransformer;
        $this->userTransformer->setEmbedParent(false);
    }

    public function transform(AbstractEntity $point)
    {
        $result = [];

        $result['id'] = $this->formatUuid($point->getId());
        $result['comment'] = $point->getComment();
        
        // This can happen only in dev database where user data is missing
        // @todo remove when user management is up and database cleared
        if (!empty($point->getUser())) {
            $result['user'] = $this->userTransformer->transform($point->getUser());
        } else {
            $result['user'] = [];
        }
        
        if ($this->embedParent) {
            $result['category']['id'] = $point->getCategory()->getId();
        }
        
        $result['tags'] = [];
        
        foreach ($point->getTags() as $tag) {
            $result['tags'][] = $this->tagTransformer->transform($tag);
        }

        return $result;
    }
}
