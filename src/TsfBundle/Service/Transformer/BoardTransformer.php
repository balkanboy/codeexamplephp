<?php

namespace TsfBundle\Service\Transformer;

use TsfBundle\Entity\AbstractEntity;

class BoardTransformer extends AbstractTransformer
{
    /**
     * @var AbstractTransformer
     */
    protected $categoryTransformer;

    public function __construct(AbstractTransformer $categoryTransformer)
    {
        $this->categoryTransformer = $categoryTransformer;
    }

    public function transform(AbstractEntity $board)
    {
        $result = [];

        $result['id'] = $this->formatUuid($board->getId());
        $result['name'] = $board->getName();
        $result['description'] = $board->getDescription();
        $result['active'] = $board->getActive();
        $result['public'] = $board->getPublic();
        $result['categories'] = [];
        
        foreach ($board->getCategories() as $category) {
            $result['categories'][] = $this->categoryTransformer->transform($category);
        }
        
        foreach ($board->getCollaborators() as $collaborator) {
            $result['editors'][] = $collaborator->getId();
        }
        
        $result['created'] = $board->getCreated()->format(\DateTime::ATOM);

        return $result;
    }
    
    public function transformForCollection(AbstractEntity $board)
    {
        $result = [];

        $result['id'] = $this->formatUuid($board->getId());
        $result['name'] = $board->getName();
        $result['description'] = $board->getDescription();
        $result['active'] = $board->getActive();
        $result['public'] = $board->getPublic();
        $result['created'] = $board->getCreated()->format(\DateTime::ATOM);

        return $result;
    }
}
