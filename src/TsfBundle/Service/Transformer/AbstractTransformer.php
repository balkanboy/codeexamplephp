<?php

namespace TsfBundle\Service\Transformer;

use TsfBundle\Entity\AbstractEntity;

/**
 * Transformation from AbstractEntity to data suitable for output.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
abstract class AbstractTransformer
{
    /**
     * @var bool
     */
    protected $embedParent = true;
    
    /**
     * Converts entity object into data suitable for API response
     * for single object.
     * 
     * @param AbstractEntity $entity
     */
    public abstract function transform(AbstractEntity $entity);
    
    /**
     * Converts entity object into data suitable for API response
     * for collection of objects.
     * 
     * Use single object transformation by default. Override in specific 
     * class if different response is needed for rendering collection.
     * 
     * @param AbstractEntity $entity
     * @return type
     */
    public function transformForCollection(AbstractEntity $entity)
    {
        return $this->transform($entity);
    }
    
    /**
     * 
     * @param string $id
     * @return string
     */
    protected function formatUuid($id)
    {
        return str_replace('-', '', $id);
    }
    
    /**
     * 
     * @return bool
     */
    public function getEmbedParent() 
    {
        return $this->embedParent;
    }

    /**
     * 
     * @param bool $embedParent
     * @return $this
     */
    public function setEmbedParent($embedParent) 
    {
        $this->embedParent = $embedParent;
        return $this;
    }


}