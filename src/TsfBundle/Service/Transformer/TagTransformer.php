<?php

namespace TsfBundle\Service\Transformer;

use TsfBundle\Entity\AbstractEntity;

class TagTransformer extends AbstractTransformer
{

    public function transform(AbstractEntity $tag)
    {
        $result = [];

        $result['id'] = $this->formatUuid($tag->getId());
        $result['name'] = $tag->getName();
        $result['default'] = $tag->getDefault();
        
        if ($this->embedParent) {
            $result['category']['id'] = $tag->getCategory()->getId();
        }

        return $result;
    }
}
