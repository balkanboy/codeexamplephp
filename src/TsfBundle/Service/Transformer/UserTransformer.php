<?php

namespace TsfBundle\Service\Transformer;

use TsfBundle\Entity\AbstractEntity;

class UserTransformer extends AbstractTransformer
{

    public function transform(AbstractEntity $user)
    {
        $result = [];

        $result['id'] = $this->formatUuid($user->getId());
        $result['firstName'] = $user->getFirstName();
        $result['lastName'] = $user->getLastName();
        $result['email'] = $user->getEmail();
        $result['name'] = $user->getName();
        
        $roles = [];

        foreach ($user->getRoles() as $userRole) {
            $result['userRoles'][] = $userRole;
        }
            
        return $result;
    }
}
