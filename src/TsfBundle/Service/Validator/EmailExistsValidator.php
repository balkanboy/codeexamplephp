<?php

namespace TsfBundle\Service\Validator;

use TsfBundle\Repository\BaseRepository;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Validates if user with supplied email exists in database.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class EmailExistsValidator extends ConstraintValidator
{
    /**
     * @var BaseRepository
     */
    protected $userRepository;
    
    public function __construct(BaseRepository $userRepository) 
    {
        $this->userRepository = $userRepository;
    }
    
    /**
     * 
     * @param string $value
     * @param Constraint $constraint
     * @return void
     * @throws UnexpectedTypeException
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value === null || $value === '') {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $users = $this->userRepository->findByEmail([$value]);

        if (!empty($users)) {
            $this->context
                ->buildViolation($constraint->getMessage())
                ->addViolation();
        }
     } 
}