<?php

namespace TsfBundle\Service\Validator\Constraint;

use TsfBundle\Service\Validator\EmailExistsValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Constraint to be used as annotation on entity fields
 * that must check if email already exists in database.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 * 
 * @Annotation
 */
class EmailUniqueConstraint extends Constraint
{
    /**
     * @var string
     */
    protected $message = 'E-mail already exists';
    
    /**
     * 
     * @return string
     */
    public function getMessage() 
    {
        return $this->message;
    }
    
    /**
     * 
     * @return ConstraintValidator
     */
    public function validatedBy()
    {
        return EmailExistsValidator::class;
    }
}
