<?php

namespace TsfBundle\Service\EventListener;

use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use TsfBundle\Service\Exception\TSFException;
use TsfBundle\Service\Response\APIResponse;

/**
 * All application - thrown exceptions will be caught here and 
 * appropriate response created.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class ExceptionListener
{
    /**
     * @var APIResponse
     */
    protected $responseRenderer;
    
    /**
     * @var string
     */
    protected $environment;

    public function __construct(APIResponse $responseRenderer, $environment)
    {
        $this->responseRenderer = $responseRenderer;
        $this->environment = $environment;
    }
    
    /**
     * 
     * @param GetResponseForExceptionEvent $event
     * @return void
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // Return if not production - we want to see output and stack trace
        // in response if we are not in production.
        if ($this->environment != 'prod') {
            return;
        }
        
        $exception = $event->getException();
        $statusCode = null;
        $headers = [];

        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
            $headers = $exception->getHeaders();
        } elseif ($exception instanceof TSFException) {
            $statusCode = $exception->getStatusCode();
        } else if ($exception instanceof InvalidUuidStringException) {
            $statusCode = Response::HTTP_BAD_REQUEST;
        } else {
            $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        } 

        $event->setResponse(
            $this->responseRenderer->renderError(
                $statusCode,
                str_replace('"', '', $exception->getMessage())
            )->setStatusCode($statusCode)
        );
    }
}