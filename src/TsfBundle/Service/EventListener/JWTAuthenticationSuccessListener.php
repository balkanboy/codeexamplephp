<?php

namespace TsfBundle\Service\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Response;

/**
 * Overrides default listener because we want to wrap response
 * into 'data' field, to be consistent with other API responses.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class JWTAuthenticationSuccessListener
{
    /**
     * 
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data['status'] = Response::HTTP_OK;
        $data['data'] = $event->getData();

        $event->setData($data);
    }
}