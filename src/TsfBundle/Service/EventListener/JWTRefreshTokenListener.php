<?php

namespace TsfBundle\Service\EventListener;

use Gesdinet\JWTRefreshTokenBundle\EventListener\AttachRefreshTokenOnSuccessListener;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

/**
 * Overrides default listener since we want to attach refresh token
 * do 'data' field and be consistent with our standard API response.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class JWTRefreshTokenListener extends AttachRefreshTokenOnSuccessListener {

    /**
     * 
     * @param AuthenticationSuccessEvent $event
     * @return void
     */
    public function onAttachRefreshToken(AuthenticationSuccessEvent $event)
    {
        parent::attachRefreshToken($event);
        $response = $event->getData();
        
        if (!isset($response['data']) || !isset($response['refresh_token'])) {
            return;
        }
        
        $response['data']['refresh_token'] = $response['refresh_token'];
        unset($response['refresh_token']);
        
        $event->setData($response);
    }
}
