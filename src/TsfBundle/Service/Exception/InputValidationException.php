<?php

namespace TsfBundle\Service\Exception;

/**
 * Exception to be thrown when input parameter is invalid.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class InputValidationException extends TSFException
{
    /**
     * {@inheritdoc}
     */
    public function getStatusCode()
    {
        return self::STATUS_CODE_BAD_INPUT;
    }
}