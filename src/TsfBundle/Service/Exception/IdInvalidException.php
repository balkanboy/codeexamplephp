<?php

namespace TsfBundle\Service\Exception;

/**
 * Exception to be thrown when ID is in invalid format.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class IdInvalidException extends TSFException
{
    /**
     * Static factory for throwing exception with formatted message.
     * 
     * @param string $uuid
     * @return IdInvalidException
     */
    public static function forId($uuid) {
        return new IdInvalidException("Invalid ID format: $uuid");
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusCode()
    {
        return self::STATUS_CODE_BAD_INPUT;
    }
}