<?php

namespace TsfBundle\Service\Exception;

/**
 * Exception to be thrown when ID is not found.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
class IdNotFoundException extends TSFException
{
    /**
     * Static factory for throwing exception with formatted message.
     * 
     * @param string $uuid
     * @return NotFoundException
     */
    public static function forId($uuid) {
        return new IdNotFoundException("ID does not exist: $uuid");
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusCode()
    {
        return self::STATUS_CODE_NOT_FOUND;
    }
}