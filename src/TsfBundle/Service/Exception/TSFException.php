<?php

namespace TsfBundle\Service\Exception;

/**
 * Generic application exception class.
 * 
 * @author Miroslav Jovanovic <mj7455@gmail.com>
 */
abstract class TSFException extends \Exception
{
    const STATUS_CODE_OK = 200;
    const STATUS_CODE_BAD_INPUT = 400;
    const STATUS_CODE_NOT_FOUND = 404;

    /**
     * Inheriting classes must implement this method to return appropriate HTTP status code.
     */
    public abstract function getStatusCode();
}