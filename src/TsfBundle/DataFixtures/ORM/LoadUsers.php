<?php

namespace TsfBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TsfBundle\Entity\User;
use TsfBundle\Entity\IdentityToken;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $user = new User();

    $user->setEmail('admin@ws.local');
    $user->setFirstName('Admin');
    $user->setLastName('Adminic');
    $user->setPassword(password_hash('bjarne', PASSWORD_BCRYPT, ['cost' => 13]));
    $user->setActive(true);

    $user->addUserRole($this->getReference('role_admin'));


    // $identity = $this->getReference('BARCODE');

    // TODO: aim is this: $user->addIdentity(Identity, token);
    // Encapsulate adding identity to User class. Identity should be factory for IdentityToken object
    // $identityToken = new IdentityToken();
    // $identityToken->setIdentity($identity);
    // $identityToken->setToken('1234asdf');
    // // $identityToken->setUser($user);
    // $user->addIdentityToken($identityToken);

    // $manager->persist($identityToken);
    $manager->persist($user);


    $terminalUser = new User();

    $terminalUser->setEmail('terminal@ws.local');
    $terminalUser->setFirstName('Terminal');
    $terminalUser->setLastName('App');
    $terminalUser->setPassword(password_hash('bjarne', PASSWORD_BCRYPT, ['cost' => 13]));
    $terminalUser->setActive(true);

    $manager->persist($terminalUser);

    $terminalUser->addUserRole($this->getReference('role_terminal'));

    $manager->flush();
  }

  public function getOrder()
  {
    return 3;
  }
}
