<?php

namespace TsfBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TsfBundle\Entity\User;
use TsfBundle\Entity\UserRole;

class LoadUserRoles extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $roles = [
        [
          'name' => 'ROLE_ADMIN',
          'description' => 'Administrator',
          'level' => 0
        ],
        [
          'name' => 'ROLE_SUPERVISOR',
          'description' => 'Supervisor - can manage own department',
          'level' => 1
        ],
        [
          'name' => 'ROLE_TERMINAL',
          'description' => 'Punch terminal app user',
          'level' => 2
        ],
        [
          'name' => 'ROLE_EMPLOYEE',
          'description' => 'Regular user',
          'level' => 3
        ],
        [
          'name' => 'ROLE_USER',
          'description' => 'Regular user',
          'level' => 4
        ],
    ];

    foreach ($roles as $role) {
      $userRole = new UserRole();

      $userRole->setName($role['name']);
      $userRole->setDescription($role['description']);
      $userRole->setLevel($role['level']);

      $manager->persist($userRole);

      if ($role['name'] === 'ROLE_ADMIN') {
        $this->addReference('role_admin', $userRole);
      }

      if ($role['name'] === 'ROLE_TERMINAL') {
        $this->addReference('role_terminal', $userRole);
      }

    }

    $manager->flush();
  }

  public function getOrder()
  {
    return 1;
  }
}
