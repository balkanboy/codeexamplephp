Code example by Miroslav Jovanovic
=======

The code in this repo is part of my project 360five.net. The purpose of this repo is to showcase my way of thinking and structuring back-end Symfony application, as well as my coding style.

It is a Symfony 3.4 application with Doctrine ORM using Postgres database. API implements bearer authentication by using JWT tokens. Authentication is not working out of the box, it requires public/private signature files to be created and added to directory var/jwt.

This repo provides Docker setup for local development. Container provides Nginx web server (with PHP FPM) and database services. Containers can be build and started with command `docker-compose up -d`.

Database stucture can be created with standard Doctrine commands:
```
php bin/console -n doctrine:migrations:diff
php bin/console -n doctrine:migrations:migrate
php php bin/console -n doctrine:fixtures:load
php app/console doctrine:schema:update --force
```

Example unit tests are provided in tests/ directory (Phpunit, Mockery).

API documentation from Swagger definitions (still work in progress): http://360five.net/apidocs/index.html

